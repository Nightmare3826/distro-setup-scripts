# Distro Setup Scripts

These are scripts that install the software that I typically install on my desktop

to install on fedora

./Fedora_setup.sh

to install on arch:

./Arch_setup.sh

To install on debian:

./debian_setup.sh

NOTE:

"dnf.conf" is part of the fedora setup and is the config file for fedora's package manager dnf, DNF by default is EXTREMLY slow, so this config adds things like always using the fastest mirrors and turning parelell downloads up to 6 instead of just one.


