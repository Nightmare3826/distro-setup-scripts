#!/bin/bash

cd

sudo apt -y update

sudo apt -y install neofetch
neofetch

cd Downloads
wget https://updates.safing.io/latest/linux_amd64/packages/portmaster-installer.deb
sudo dpkg -i portmaster-installer.deb
cd

sudo apt -y install nala

sudo nala install gimp kdenlive inkscape zsh fish htop curl git flatpak

git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install

git clone https://gitlab.com/dwt1/wallpapers.git


