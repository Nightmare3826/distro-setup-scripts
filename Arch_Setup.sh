#!/bin/bash

sudo pacman -S neofetch

neofetch

git clone https://github.com/safing/portmaster-packaging

cd portmaster-packaging/linux
makepkg -is
cd

sudo systemctl daemon-reload
sudo systemctl enable --now portmaster

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install

sudo pacman -S gimp

sudo pacman -S kdenlive

sudo pacman -S obs-studio

sudo pacman -S obsidian

sudo pacman -S blender

sudo pacman -S inkscape

flatpak install -y flathub com.rafaelmardojai.Blanket

flatpak install -y flathub de.haeckerfelix.Shortwave

flatpak install -y flathub io.github.seadve.Mousai

flatpak install -y flathub fr.romainvigier.MetadataCleaner

flatpak install -y flathub org.gnome.DejaDup

flatpak install -y flathub de.haeckerfelix.Fragments

flatpak install -y flathub org.gustavoperedo.FontDownloader

flatpak install -y flathub com.github.maoschanz.drawing

flatpak install -y flathub org.gnome.Podcasts

flatpak install -y flathub com.gitlab.newsflash

yay -S brave-bin

sudo pacman -S gnome-tweaks

