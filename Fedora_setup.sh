#!/bin/bash

cd


sudo dnf -y update

cd $HOME/distro-setup-scripts
sudo rm /etc/dnf/dnf.conf
sudo cp dnf.conf /etc/dnf/dnf.conf

sudo dnf -y install neofetch
neofetch

sudo dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm && sudo dnf -y groupupdate core && sudo dnf -y groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin && sudo dnf -y groupupdate sound-and-video

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak remote-add --if-not-exists fedora oci+https://registry.fedoraproject.org

cd $HOME/Downloads/
wget https://updates.safing.io/latest/linux_amd64/packages/portmaster-installer.rpm
sudo dnf install portmaster-installer.rpm
cd

sudo dnf -y install gimp kdenlive obs-studio blender vim neovim emacs

flatpak install -y flathub md.obsidian.Obsidian

flatpak install -y flathub com.rafaelmardojai.Blanket

flatpak install -y flathub de.haeckerfelix.Shortwave

flatpak install -y flathub io.github.seadve.Mousai

flatpak install -y flathub fr.romainvigier.MetadataCleaner

flatpak install -y flathub org.gnome.DejaDup

flatpak install -y flathub de.haeckerfelix.Fragments

flatpak install -y flathub org.gustavoperedo.FontDownloader

flatpak install -y flathub com.github.maoschanz.drawing

flatpak install -y flathub org.gnome.Podcasts

flatpak install -y flathub com.gitlab.newsflash

git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install

sudo dnf -y install gnome-tweaks

sudo dnf -y install dnf-plugins-core

sudo dnf -y config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/

sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc

sudo dnf -y install brave-browser


